$(document).ready(function () {
    var array = [];
    if (typeof (Storage) !== "undefined") {
        if (localStorage.getItem("testObject") !== null) {
            $("#recovery-list").html('');
            var retrievedObject = localStorage.getItem('testObject');
            var hede = JSON.parse(retrievedObject);
            hede.forEach(element => {
                $.get(element.url, function (lResponse) {
                    if (lResponse.Response) {
                        var MovieContent = '';
                        var rating = '';
                        if (lResponse.imdbRating <= 4.9 || lResponse.imdbRating == 'N/A') {
                            rating = 'red';
                        } else if (lResponse.imdbRating > 5.0 || lResponse.imdbRating <= 9.9) {
                            rating = 'yellow';
                        } else {
                            rating = 'green';
                        }
                        if (lResponse.Poster === 'N/A') {
                            imgUrl = "https://picsum.photos/id/1038/300/444";
                        } else {
                            imgUrl = lResponse.Poster
                        }
                        MovieContent += '<div class="list-item" item-title="' + element.title + '"><div class="remove-item">x</div><div class="item-poster"><img src="' + imgUrl + '" /></div> <div class="item-title">' + lResponse.Title + '</div><div class="item-score ' + rating + '">' + lResponse.imdbRating + '</div></div>';
                        $('#recovery-list').append(MovieContent);
                    }
                });
            });
        }
        $(document).on('click', '.remove-item', function () {
            $(this).parent().hide();
            var parentTite = $(this).parent().attr("item-title");
            var retrievedObject = localStorage.getItem('testObject');
            var hede = JSON.parse(retrievedObject);
            var hede = $.grep(hede, function (e) {
                return e.title != parentTite;
            });
            localStorage.setItem('testObject', JSON.stringify(hede));
        });
        $("#search-button").click(function () {
            var searchTitle = $("#search-input").val();
            var nUrl = "http://www.omdbapi.com/?apikey=63f944af&t=" + searchTitle
            if (localStorage.getItem("testObject") !== null) {
                var retrievedObjectx = localStorage.getItem('testObject');
                var array = JSON.parse(retrievedObjectx);
            }
            array.push({ title: searchTitle, url: nUrl });
            $.get(nUrl, function (response) {
                if (response.Response) {
                    $("#list").html('');
                    var MovieContent = '';
                    var rating = '';
                    if (response.imdbRating <= 4.9 || response.imdbRating == 'N/A') {
                        rating = 'red';
                    } else if (response.imdbRating > 5.0 || response.imdbRating <= 9.9) {
                        rating = 'yellow';
                    } else {
                        rating = 'green';
                    }
                    if (response.Poster === 'N/A') {
                        imgUrl = "https://picsum.photos/id/1038/300/444";
                    } else {
                        imgUrl = response.Poster
                    }
                    MovieContent += '<div class="list-item"><div class="item-poster"><img src="' + imgUrl + '" /></div> <div class="item-title">' + response.Title + '</div><div class="item-score ' + rating + '">' + response.imdbRating + '</div></div>';
                    $('#list').append(MovieContent);
                }
            });
            localStorage.setItem('testObject', JSON.stringify(array));
            console.log(localStorage.getItem('testObject'));
        });

    } else {
        // Sorry! No Web Storage support..
    }

});
